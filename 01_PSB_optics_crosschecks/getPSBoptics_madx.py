#%%
import numpy as np
import matplotlib.pyplot as plt
from cpymad.madx import Madx
import pandas as pd
import pickle


#%%
mad = Madx()
mad.globals.thin = 1
mad.globals.slices = 3
mad.call('psb_flat_bottom.madx')
mad.input('twiss;')
myTwiss=mad.table.twiss.dframe()
myTwiss.to_pickle('outputs/madx_twiss.pkl')