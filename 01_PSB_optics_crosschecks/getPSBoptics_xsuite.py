#%%
import xtrack as xt
import xpart as xp
import xobjects as xo
import xfields as xf

import numpy as np
import matplotlib.pyplot as plt
from cpymad.madx import Madx

import optics.psb_lattice as psb


#%%
####################
# Choose a context #
####################
context = xo.ContextCpu()
# context = xo.ContextCupy()
#context = xo.ContextPyopencl('0.0')
print(context)


#%%
#############################
# Load PSB line
#############################
mad = Madx()
#--------------------------------------------------
# Load PSB lattice from local .madx file
# It includes a dummy cavity
#--------------------------------------------------
mad.globals.thin = 1
mad.globals.slices = 3
mad.call('psb_flat_bottom.madx')
line= xt.Line.from_madx_sequence(mad.sequence['psb1'],
                                 apply_madx_errors=True,
                                 deferred_expressions=True)
line.particle_ref=xp.Particles(mass0=xp.PROTON_MASS_EV,gamma0=mad.sequence.psb1.beam.gamma)
#--------------------------------------------------
# Load PSB lattice optics package
# It does not include cavity
#--------------------------------------------------
#twtable = psb.get_MADX_lattice(mad, QH_target=4.40, QV_target=4.60, year='2022', 
#                               makethin=True, refer='center',slice=3)
#line = xt.Line.from_madx_sequence(mad.sequence['psb1'])
#line.particle_ref=xp.Particles(mass0=xp.PROTON_MASS_EV, gamma0=mad.sequence.psb1.beam.gamma)
#cavs = [ee for ee in line.elements if isinstance(ee,xt.Cavity)]
#for cc in cavs:
#    #cc.voltage = 8e3
#    cc.voltage = 0.00293405*1e6
#    #cc.lag = 180 # phase
#    cc.lag = np.pi
#    cc.frequency = mad.sequence.psb1.beam.beta*const.c/157.08
#    print(cc.to_dict())


#%%
#############################
# Setup parameters
#############################
nemitt_x=1.5e-6
nemitt_y=1e-6
bunch_intensity=50e10
sigma_z=16.9
num_turns=1 # number of turns to track
num_spacecharge_interactions = 160 # per turn
tol_spacecharge_position = 1e-2 #is this the minimum/maximum space between sc elements?
mode = 'frozen' # available modes: 'frozen', 'quasi-frozen', 'pic'


#%%
#############################################
# Install spacecharge interactions (frozen) #
#############################################
'''
lprofile = xf.LongitudinalProfileQGaussian(
        number_of_particles=bunch_intensity,
        sigma_z=sigma_z, z0=0., q_parameter=1.)

xf.install_spacecharge_frozen(line=line,
                   particle_ref=line.particle_ref,
                   longitudinal_profile=lprofile,
                   nemitt_x=nemitt_x, nemitt_y=nemitt_y,
                   sigma_z=sigma_z,
                   num_spacecharge_interactions=num_spacecharge_interactions,
                   tol_spacecharge_position=tol_spacecharge_position)
'''

#%%
#################
# Build Tracker
# Get optics from Tracker
#################
freeze_vars = xp.particles.part_energy_varnames() + ['zeta']
tracker = xt.Tracker(_context=context,line=line,
                     #local_particle_src=xp.gen_local_particle_api(freeze_vars=freeze_vars)
                    )
#tracker_sc_off = tracker.filter_elements(exclude_types_starting_with='SpaceCh')
#tw = tracker.twiss(method='4d') # Need to choose 4d method when longitudinal variables are frozen
twtable = tracker.twiss() # Need to choose 4d method when longitudinal variables are frozen
twtable = twtable.to_pandas()
#twtable.to_pickle('outputs/xsuite_twiss.pkl')
#twtable.to_pickle('outputs/xsuite_twiss_withFrozenSpaceCharge.pkl')


#%%
#################
# Alternative way to get optics
# Line needs not to be associated with a tracker
# Same result as above
#################
tr = line.build_tracker()
twtable = tr.twiss().to_pandas()
#twtable.to_pickle('outputs/xsuite_twiss_fromLine.pkl')

# %%
