#%%
import numpy as np
import matplotlib.pyplot as plt
import pickle
import pandas as pd
from scipy.interpolate import interp1d


#%%
madx_twiss = pd.read_pickle('outputs/madx_twiss.pkl')
xsuite_twiss = pd.read_pickle('outputs/xsuite_twiss.pkl')
xsuite_twiss_fromLine = pd.read_pickle('outputs/xsuite_twiss_fromLine.pkl')
xsuite_twiss_withFrozenSpaceCharge = pd.read_pickle('outputs/xsuite_twiss_withFrozenSpaceCharge.pkl')


#%%
# Compare xsuite_twiss and xsuite_twiss_fromLine
fig,ax = plt.subplots(1,1,figsize=(8,4))
key = 'bety'
s = xsuite_twiss['s']
ax.plot(s, xsuite_twiss[key]-xsuite_twiss_fromLine[key])


#%%
# Without space charge
fig,ax = plt.subplots(1,1,figsize=(8,4))
fontsize=15
ax.set_xlabel('s [m]',fontsize=fontsize)
ax.set_ylabel(r'$\beta_y^{{madx}} - \beta_y^{{xsuite}}$ (%)',fontsize=fontsize)
ax.tick_params(labelsize=fontsize)

key = 'bety'
s = madx_twiss['s']
f = interp1d(xsuite_twiss['s'], xsuite_twiss[key], kind='linear', fill_value="extrapolate")
ax.plot(s, (madx_twiss[key]-f(s))/madx_twiss[key]*100)
#ax.plot(madx_twiss['s'],madx_twiss['bety'])
#ax.plot(xsuite_twiss['s'],xsuite_twiss['bety'])
#ax.set_xlim(0,65)
fig.tight_layout()
#fig.savefig('outputs/optics_001_xsuite-madx_bety_relative.png', dpi=400)
plt.show()


# %%
# With space charge
fig,ax = plt.subplots(1,1,figsize=(8,4))
fontsize=15
ax.set_xlabel('s [m]',fontsize=fontsize)
ax.set_ylabel(r'$\beta_y^{{madx}} - \beta_y^{{xsuite}}$ (%)',fontsize=fontsize)
ax.tick_params(labelsize=fontsize)

key = 'bety'
s = madx_twiss['s']
f = interp1d(xsuite_twiss_withFrozenSpaceCharge['s'], xsuite_twiss_withFrozenSpaceCharge[key], kind='linear', fill_value="extrapolate")
ax.plot(s, (madx_twiss[key]-f(s))/madx_twiss[key]*100)
#ax.plot(madx_twiss['s'],madx_twiss['bety'])
#ax.plot(xsuite_twiss['s'],xsuite_twiss['bety'])
#ax.set_xlim(0,65)
fig.tight_layout()
#fig.savefig('outputs/optics_002_xsuite-madx_bety_relative_withFrozenSpaceCharge.png', dpi=400)
plt.show()
# %%
