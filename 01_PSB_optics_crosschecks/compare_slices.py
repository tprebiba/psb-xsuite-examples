#%%
import numpy as np
import matplotlib.pyplot as plt
from cpymad.madx import Madx
import pandas as pd
from scipy.interpolate import interp1d


#%%
# thick lattice
mad = Madx()
mad.globals.thin=0 # keeps lattice thick
mad.call('psb_flat_bottom.madx')
mad.input('twiss, centre=True;')
twiss_thick=mad.table.twiss.dframe()
twiss_thick_summ = mad.table.summ.dframe()


#%%
twiss_thin = {}
twiss_thin_summ = {}
for slice in ['1','2','3','4','5']:
    mad = Madx()
    mad.globals.thin=1
    mad.globals.slices = int(slice)
    mad.call('psb_flat_bottom.madx')
    mad.input('twiss, centre=True;') # all elements thin; centre=True not needed
    twtable = mad.table.twiss
    twiss_thin[slice+'slices']=mad.table.twiss.dframe()
    twiss_thin_summ[slice+'slices']=mad.table.summ.dframe()


#%%
key2compare = 'bety'
#key2compare = 'dx'
#key2compare = 'betx'


#%%
plot_absolute = True
if plot_absolute:
    fig,ax = plt.subplots(1,1,figsize=(8,4))
    fontsize=12
    ax.set_xlabel('s [m]',fontsize=fontsize)
    ax.set_ylabel(r'$\beta_y$ [m]',fontsize=fontsize)
    ax.tick_params(labelsize=fontsize)
    ax.plot(twiss_thick['s'], twiss_thick[key2compare], c='black', label='Thick',lw=2)
    for slice in ['1','2','3','4','5']:
        plt.plot(twiss_thin['%sslices'%slice]['s'],twiss_thin['%sslices'%slice][key2compare],
                label='# of slices: %s'%slice,lw=2)
    #ax.set_xlim(48,62)
    ax.set_xlim(54,56)
    ax.legend(loc=0)
    fig.tight_layout()
    #fig.savefig('outputs/slices_001_bety.png', dpi=400)
    plt.show()


#%%
plot_relative = True
if plot_relative:
    fig,ax = plt.subplots(1,1,figsize=(8,4))
    fontsize=12
    ax.set_xlabel('s [m]',fontsize=fontsize)
    ax.set_ylabel(r'$\beta_y^{{thick}} - \beta_y^{{thin}}$ (%)',fontsize=fontsize)
    ax.tick_params(labelsize=fontsize)
    s = twiss_thick['s']
    for slice in ['1','2','3','4','5']:
        f = interp1d(twiss_thin['%sslices'%slice]['s'], twiss_thin['%sslices'%slice][key2compare], kind='linear', fill_value="extrapolate")
        deltabeta = (twiss_thick[key2compare]-f(s))/twiss_thick[key2compare]*100
        ax.plot(s,deltabeta,label='# of slices: %s (RMS %1.1f %%)'%(slice, np.std(deltabeta)))
    ax.legend(loc=0)
    fig.tight_layout()
    #fig.savefig('outputs/slices_002_bety_relative.png', dpi=400)
    plt.show()


# %%
plot_convergence = True
if plot_convergence:
    fig,ax = plt.subplots(1,1,figsize=(8,4))
    fontsize=12
    ax.set_xlabel('Number of slices',fontsize=fontsize)
    ax.set_ylabel(r'$\beta_y^{{thick}} - \beta_y^{{thin}}$ (%)',fontsize=fontsize)
    ax.tick_params(labelsize=fontsize)
    slices = [1,2,3,4,5]
    _rms = []
    _mean = []
    _max = []
    for slice in slices:
        slice = str(slice)
        f = interp1d(twiss_thin['%sslices'%slice]['s'], twiss_thin['%sslices'%slice][key2compare], kind='linear', fill_value="extrapolate")
        s = twiss_thick['s']
        deltabeta = (twiss_thick[key2compare]-f(s))/twiss_thick[key2compare]*100
        _rms.append(np.std(deltabeta))
        _mean.append(np.mean(deltabeta))
        _max.append(np.max(np.abs(deltabeta)))
    ax.plot(slices, _rms, '.-', c='blue', label='RMS')
    ax.plot(slices, _mean, '.-', c='green', label='Mean')
    ax.plot(slices, _max, '.-', c='red', label='Max')
    ax.legend(loc=0, fontsize=fontsize)
    fig.tight_layout()
    #fig.savefig('outputs/slices_003_bety_convergence.png', dpi=400)
    plt.show()


#%%
#key = 'dq1'
key = 'dq2'
#key = 'gammatr'
#key = 'alfa'
#key = 'betymax'
#print(twiss_thick_summ.keys())
plot_summ = True
if plot_summ:
    fig,ax = plt.subplots(1,1,figsize=(8,4))
    fontsize=12
    ax.set_xlabel('Number of slices',fontsize=fontsize)
    ax.set_ylabel(key,fontsize=fontsize)
    ax.tick_params(labelsize=fontsize)
    ax.axhline(twiss_thick_summ[key].values[0], ls='--', lw=2,c='black', label='Thick')
    for slice in ['1','2','3','4','5']:
        ax.plot(int(slice), twiss_thin_summ[slice+'slices'][key].values[0], '*', ms=15, label='Thin: %s slice(s)'%slice)
    ax.legend(loc=0, fontsize=fontsize)
    fig.tight_layout()
    #fig.savefig('outputs/slices_004_dq1.png', dpi=400)
    #fig.savefig('outputs/slices_005_dq2.png', dpi=400)
    #fig.savefig('outputs/slices_006_gammatr.png', dpi=400)
    #fig.savefig('outputs/slices_007_alfa.png', dpi=400)
    #fig.savefig('outputs/slices_008_betymax.png', dpi=400)
    plt.show()

# %%
