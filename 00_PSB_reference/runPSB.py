#%%
import xtrack as xt
import xpart as xp
import xobjects as xo
import xfields as xf

import numpy as np
import matplotlib.pyplot as plt
from cpymad.madx import Madx

import optics.psb_lattice as psb


#%%
####################
# Choose a context #
####################
context = xo.ContextCpu()
# context = xo.ContextCupy()
#context = xo.ContextPyopencl('0.0')


#%%
#############################
# Load PSB line
#############################
#--------------------------------------------------
# Load PSB lattice from local .madx file
# Is thin and it includes a dummy cavity
#--------------------------------------------------
line = xt.Line.from_json('psb/psb_line.json')
#--------------------------------------------------
# Load PSB lattice optics package
# It does not include cavity
#--------------------------------------------------
#mad = Madx()
#twtable = psb.get_MADX_lattice(mad, QH_target=4.40, QV_target=4.60, year='2022', 
#                               makethin=True, refer='center',slice=3)
#line = xt.Line.from_madx_sequence(mad.sequence['psb1'])
#line.particle_ref=xp.Particles(mass0=xp.PROTON_MASS_EV, gamma0=mad.sequence.psb1.beam.gamma)
#cavs = [ee for ee in line.elements if isinstance(ee,xt.Cavity)]
#for cc in cavs:
#    #cc.voltage = 8e3
#    cc.voltage = 0.00293405*1e6
#    #cc.lag = 180 # phase
#    cc.lag = np.pi
#    cc.frequency = mad.sequence.psb1.beam.beta*const.c/157.08
#    print(cc.to_dict())


#%%
#############################
# Setup parameters
#############################
nemitt_x=1.5e-6
nemitt_y=1e-6
bunch_intensity=50e10
sigma_z=16.9
num_turns=1 # number of turns to track
num_spacecharge_interactions = 160 # per turn
tol_spacecharge_position = 1e-2 #is this the minimum/maximum space between sc elements?
mode = 'frozen' # available modes: 'frozen', 'quasi-frozen', 'pic'


#%%
#############################################
# Install spacecharge interactions (frozen) #
#############################################
lprofile = xf.LongitudinalProfileQGaussian(
        number_of_particles=bunch_intensity,
        sigma_z=sigma_z, z0=0., q_parameter=1.)

xf.install_spacecharge_frozen(line=line,
                   particle_ref=line.particle_ref,
                   longitudinal_profile=lprofile,
                   nemitt_x=nemitt_x, nemitt_y=nemitt_y,
                   sigma_z=sigma_z,
                   num_spacecharge_interactions=num_spacecharge_interactions,
                   tol_spacecharge_position=tol_spacecharge_position)


#%%
#################
# Build Tracker #
#################
freeze_vars = xp.particles.part_energy_varnames() + ['zeta']
tracker = xt.Tracker(_context=context,line=line,
                     #local_particle_src=xp.gen_local_particle_api(freeze_vars=freeze_vars)
                    )
#tracker_sc_off = tracker.filter_elements(exclude_types_starting_with='SpaceCh')
tw = tracker.twiss(method='4d') # Need to choose 4d method when longitudinal variables are frozen


#%%
######################
# Generate particles #
######################
n_part = int(2e2)
'''
x_norm, y_norm, _, _ = xp.generate_2D_polar_grid(
    theta_range=(0.01, np.pi/2-0.01),
    ntheta = 20,
    r_range = (0.1, 7),
    nr = 30) # naprticles = ntheta*nr
particles = xp.build_particles(tracker=tracker, particle_ref=line.particle_ref,
                               x_norm=x_norm, y_norm=y_norm, delta=0,
                               scale_with_transverse_norm_emitt=(nemitt_x, nemitt_y),
                               method='4d')
'''
particles = xp.build_particles(tracker=tracker, particle_ref=line.particle_ref,
                        x=np.random.normal(0, 2e-3, n_part),
                        px=np.random.normal(0, 1.1e-3, n_part),
                        y=np.random.normal(0, 2e-3, n_part),
                        py=np.random.normal(0, 1.1e-3, n_part),
                        zeta=np.random.uniform(-1e-2, 1e-2, n_part),
                        delta=np.random.uniform(-1e-4, 1e-4, n_part),
                        method='4d')


#%%
#############################
# Match tunes and Twiss it
#############################
qx_target = 4.40
qy_target = 4.45
tracker.match(
              vary=[
                    xt.Vary('kbrqf', step=1e-8),
                    xt.Vary('kbrqd', step=1e-8),
              ],
              targets = [
                         xt.Target('qx', qx_target, tol=1e-5),
                         xt.Target('qy', qy_target, tol=1e-5)
              ]
)
twbefore = tracker.twiss().to_pandas()


#%%
#############################
# Start tracking
#############################
tracker.track(particles, num_turns=num_turns, #turn_by_turn_monitor=True,
                                              turn_by_turn_monitor='ONE_TURN_EBE'
              )
#np.save('outputs/s', tracker.record_last_track.s[0])
#np.save('outputs/x',tracker.record_last_track.x)
#np.save('outputs/px',tracker.record_last_track.px)
#np.save('outputs/y',tracker.record_last_track.y)
#np.save('outputs/py',tracker.record_last_track.py)
#np.save('outputs/z',tracker.record_last_track.zeta)
#np.save('outputs/dp',tracker.record_last_track.delta)


# %%
