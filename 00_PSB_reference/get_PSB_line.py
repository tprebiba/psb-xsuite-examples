#%%
from cpymad.madx import Madx
import xtrack as xt
import xpart as xp
import xobjects as xo
import xfields as xf
from xtrack.slicing import Strategy, Teapot
import numpy as np


#%%
context = xo.ContextCpu()
mad = Madx()
mad.globals.thin = 1 # if > 0 madx makes all elements thin
xsuite_slicing = False
mad.globals.slices = 3
mad.call('psb_flat_bottom.madx')
line= xt.Line.from_madx_sequence(mad.sequence['psb1'],
                                 deferred_expressions=True,
                                 install_apertures=True, # apertures are not yet supported for thick elements
                                 enable_field_errors=True, # field errors are not yet supported for thick elements
                                 enable_align_errors=True,
                                 allow_thick=False,
                                 use_compound_elements=True)
line.particle_ref=xp.Particles(mass0=xp.PROTON_MASS_EV,gamma0=mad.sequence.psb1.beam.gamma)
if mad.globals.thin == 0:
    #line.configure_bend_model(core='full', edge='full') # switch to exact model for bends (PTC-like, appropriate for small rings) 
    line.configure_bend_model(core='expanded', edge='full')
# Slice thick line with xsuite
if ((mad.globals.thin == 0) and (xsuite_slicing)):
      line.slice_thick_elements(
           slicing_strategies=[
                Strategy(slicing=Teapot(3)), # default is 1
                Strategy(slicing=Teapot(3), element_type=xt.Bend),
                Strategy(slicing=Teapot(3), element_type=xt.Quadrupole),
                Strategy(slicing=Teapot(3), name=r'bi.*bsw.*'),
           ]
      )
# Inspect elements
line_table = line.get_table()
#line_table.rows[:12]
#line.element_names
#line.element_dict


#%%
#############################
# Modify line
#############################
# Make a new line starting from a different element
#line.discard_tracker() # if line is modified, tracker (if built) must be discarded
#line.cycle(index_first_element=index, inplace=True) # based on index
#line.cycle(name_first_element = 'bi1.tstr1l1', inplace=True) # injection foil
#line.cycle(name_first_element = 'br1.bwsv11l1', inplace=True) # vertical (LIU) wire scanner
line.build_tracker(_context=context)
qx_target = 4.17
qy_target = 4.23
line.match(
              vary=[
                    xt.Vary('kbrqf', step=1e-8),
                    xt.Vary('kbrqd', step=1e-8),
              ],
              targets = [
                         xt.Target('qx', qx_target, tol=1e-5),
                         xt.Target('qy', qy_target, tol=1e-5)
              ]
)


#%%
#############################
# Twiss it
#############################
#line.twiss_default['method'] = '4d' # if no RF present, default is '6d
#line.twiss_default['group_compound_elements'] = True # to hide group "compound" elements
tw = line.twiss()
tw.rows[20:25].cols['s', 'betx', 'bety'] # based on index
tw.rows[0.0:10.0:'s'].cols['s', 'betx', 'bety'] # based on s position
tw.rows['bi1.tstr1l1':'br1.bwsv11l1'].cols['s', 'betx', 'bety'] # based on element name
#tw.rows['.*bws.*']
twdf = tw.to_pandas()


#%%
#############################
# Get/set variables and dependencies
#############################
# Note: in psb_flat_bottom.madx, RFs are removed and a single one is installed BR.C02
element_mask = np.where(line_table['element_type']=='Cavity')[0]
print(line_table.rows[element_mask])
print(line.element_dict['br.c02'])
#line.vars['v_rf'] = 8e3 # in V, note: madx uses kV
#line.vars['f_rf'] = 1/tw.T_rev0 # in Hz
#line.element_refs['br.c02'].voltage = line.vars['v_rf']
#line.element_refs['br.c02'].frequency = line.vars['f_rf']
print(line.vars['kbrqd']._value)
print(line.vars['kbrqd']._info())
print(line.vars['kbrqf']._value)
print(tw.rows['.*qde3.*'])
print(line.element_dict['br.qde3..1'])
print(line.element_refs['br.qde3..1'].knl._info())
print(line.element_refs['br.qde3..1'].knl[1]._info())


#%%
#############################
# Save some lines
#############################
#line.to_json('psb/psb_line_4p17_4p23_thick.json') # no aperture, no field errors
#line.to_json('psb/psb_line_4p40_4p45_thick.json') # no aperture, no field errors
#line.to_json('psb/psb_line_4p17_4p23_madthin3.json')
#line.to_json('psb/psb_line_4p40_4p45_madthin3.json')
#line.to_json('psb/psb_line_4p17_4p23_madthin3_at_tstr1l1.json')
#line.to_json('psb/psb_line_4p17_4p23_madthin3_at_bwsv11l1.json')
#line.to_json('psb/psb_line_4p17_4p23_xsuitethin3.json')
#line.to_json('psb/psb_line_4p40_4p45_xsuitethin3.json')
# %%
