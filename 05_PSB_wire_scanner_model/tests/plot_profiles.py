#%%
import glob
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


#%%
df = {}
for flag in ['before', 'after']:

    file = glob.glob(f'outputs/particles_{flag}_wire_scan.json')
    df[flag] = pd.read_json(file[0])
df['wire_scanner_signal'] = pd.read_json('outputs/wire_scanner_signal.json')

#%%

f, axs = plt.subplots(2,1,figsize=(5,10),facecolor='white')
fontsize=10

ax=axs[0]
ax.set_xlabel('y [mm]', fontsize=fontsize)
ax.set_ylabel('py [mrad]', fontsize=fontsize)
ax.tick_params(labelsize=fontsize)
ax.plot(df['after']['y']*1e3, df['after']['py']*1e3, '.', ms=0.5, label='After', c='red')
ax.plot(df['before']['y']*1e3, df['before']['py']*1e3, '.', ms=0.5, label='Before', c='blue')
ax.legend(loc=0, fontsize=fontsize-3)

ax=axs[1]
ax.set_xlabel('y [mm]', fontsize=fontsize)
ax.set_ylabel('Ampl. [a.u.]', fontsize=fontsize)
ax.tick_params(labelsize=fontsize)
wire_pos = df['wire_scanner_signal']['ws_pos']
wire_signal = df['wire_scanner_signal']['ws_signal']
p1, bin_edges = np.histogram(df['before']['y'], wire_pos, normed=True) # review normed
ax.plot(wire_pos[0:-1]*1e3, p1, label='Before',c='blue')
p2, bin_edges = np.histogram(df['after']['y'], wire_pos, normed=True) # review normed
ax.plot(wire_pos[0:-1]*1e3, p2, label='After', c='red')
ax.plot(wire_pos*1e3, wire_signal, label='Wire signal (simulated)', c='green')
ax.legend(loc=0, fontsize=fontsize-3)

plt.show()

# %%
'''
particles_coord = np.arange(-0.02, 0.02, 1e-7)
height = 1
wire_d = 38e-6
current_wire_pos = 0
square = np.heaviside(particles_coord-(current_wire_pos-wire_d/2),height) - np.heaviside(particles_coord-(current_wire_pos+wire_d/2),height)
k = 10/wire_d
wire_d2 = wire_d*(1+1/(k*wire_d)**2)
height2 = height*(1+1/(k*wire_d))
smothed = ((height2/(1+np.exp(-k*(particles_coord-(current_wire_pos-wire_d2/2))))) - (height2/(1+np.exp(-k*(particles_coord-(current_wire_pos+wire_d2/2))))))
circle_chord = 2*np.sqrt((wire_d/2)**2-(particles_coord-current_wire_pos)**2)/wire_d
plt.plot(particles_coord, square)
plt.plot(particles_coord, smothed)
plt.plot(particles_coord, circle_chord)
plt.xlim(-1e-4, 1e-4)
'''
# %%