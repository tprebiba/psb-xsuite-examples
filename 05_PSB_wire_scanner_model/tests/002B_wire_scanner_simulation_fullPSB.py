import beamtools.wire_scanner as bws
import json
import numpy as np
import matplotlib.pyplot as plt
import xtrack as xt
import xpart as xp
import xobjects as xo
import time


n_part = int(1e4) #int(6e5)
##################################################
# Get Particles from Xsuite
##################################################
context = xo.ContextCpu()
line = xt.Line.from_json('psb_line_4p17_4p23_BWSV11L1.json')
line.build_tracker(_context=context)
particles = xp.generate_matched_gaussian_bunch(num_particles=n_part, total_intensity_particles=10e10, 
                                               nemitt_x=0.45e-6, nemitt_y=0.45e-6, sigma_z=(271/4)*0.525*0.3,
                                               line=line)


# %%
##################################################
# Create a wire scanner
##################################################
wire_d = 38e-6 # in m, typical value for the PSB
theta=0.47e-3 # in rad, typical value for the PSB
with open('PSB_MD5465_R3V_C290_avg_profile.json','r') as f:
    data = json.load(f)
# minus sign: I change the IN scan (right to left) to an OUT (left to right) scan to aggree with the definition of the ws.profile() function
# it is equivalent nevertheless
wire_pos = -np.array(data['mean_pos'])*1e-3 # real wire displacement in the PSB (not uniform speed)
#wire_pos = np.arange(-0.02, 0.02, 0.00003) # constant speed at 30 m/s (uniform wire displacement)
# Reducing the wire_pos to fasten the simulation
mask = np.where((wire_pos<0.01) & (wire_pos>-0.01))
wire_pos = wire_pos[mask]
ws = bws.WireScanner(
                     wire_d=wire_d, wire_shape='square', 
                     theta=theta, angle_distribution='gaussian',
                     wire_pos=wire_pos,
                     #wire_speed = 30 # in m/s, optional, if >0 will re-define wire_pos
                     plane='y'
                     )


# %%
##################################################
# Simulate wire scan
##################################################
v_profile_init = ws.profile(particles, bins_flag='non-uniform')
start = time.process_time()
num_turns = len(wire_pos)
for ii in range(num_turns):
    print(f'turn {ii}/{num_turns}')

    # track one turn using xsuite
    line.track(particles, 
               freeze_longitudinal=True,
               #turn_by_turn_monitor=True
               )

    # displace wire and make scattering
    ws.set_wire_pos(ii)
    ws.track(particles)

    # save distribution after wire scan
    if ii==(num_turns-1):
        print(f'save turn {ii}')
        #with open(f'outputs/particles_after_wire_scan.json', 'w') as fid:
        #    json.dump(particles.to_dict(), fid, cls=xo.JEncoder)

        # save also the WS signal
        #with open(f'outputs/wire_scanner_signal.json', 'w') as fid:
        #    json.dump({'ws_signal':list(ws.ws_signal_amp),
        #               'ws_pos': list(ws.ws_signal_pos)}, 
        #               fid) 

print(time.process_time()-start)
v_profile_final = ws.profile(particles, bins_flag='non-uniform')


##################################################
# Plotcs
##################################################
f,ax = plt.subplots(1,1,figsize=(6,5))
fontsize=15
lw=2.0
ax.set_ylabel('Amplitude [a.u.]', fontsize=fontsize)
ax.set_xlabel('y [mm]', fontsize=fontsize)
ax.tick_params(labelsize=fontsize)
ax.tick_params(axis='y', labelleft=False, left=False)
ax.plot(wire_pos*1e3, v_profile_init, '-', lw=lw, c='blue', label='Initial')
ax.plot(wire_pos*1e3, v_profile_final, '-', lw=lw, c='red', label='Final')
ax.plot(np.array(ws.ws_signal_pos)*1e3, ws.ws_signal_amp, '-', lw=lw, c='green', label='WS signal')
ax.legend(loc=0, fontsize=fontsize-3)
f.tight_layout()
plt.show()