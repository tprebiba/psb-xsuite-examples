import json
import xobjects as xo
import xpart as xp
import matplotlib.pyplot as plt
import numpy as np

import sys
import os
sys.path.insert(0, os.path.relpath('../'))
from foil import Foil

####################################################################
# PSB foil
####################################################################
thickness = 200 #ug/cm^2
xmin = -0.099
#xmax = -0.067
xmax = +0.099 # for testing
ymin = -0.029
ymax = 0.029
psbfoil = Foil(xmin, xmax, ymin, ymax, thickness)


####################################################################
# A test distribution
####################################################################
with open('testparticles.json', 'r') as fid:
    particles = xp.Particles.from_dict(json.load(fid), _context=xo.ContextCpu())


turns2cross = 10


####################################################################
# Test simple scatter
####################################################################
scatterchoice = 1 # 1: simple (no losses) 0: full (with losses)
psbfoil.setScatterChoice(scatterchoice)
particles1 = particles.copy()
for i in range(turns2cross):
    print(i)
    psbfoil.track(particles1)
                  

####################################################################
# Test full scatter
####################################################################
scatterchoice = 0 # 1: simple (no losses) 0: full (with losses)
psbfoil.setScatterChoice(scatterchoice)
particles2 = particles.copy()
for i in range(turns2cross):
    print(i)
    psbfoil.track(particles2)


####################################################################
# Plots
####################################################################
print(particles2.state)
plt.plot(particles.x*1e3, particles.px*1e3, '.', label='initial')
plt.plot(particles1.x*1e3, particles1.px*1e3, '.', label='simple scatter')
plt.plot(particles2.x*1e3, particles2.px*1e3, '.', label='full scatter')
plt.legend(loc=0)
plt.ylim(-8,8)
plt.xlim(-12,12)
plt.show()

