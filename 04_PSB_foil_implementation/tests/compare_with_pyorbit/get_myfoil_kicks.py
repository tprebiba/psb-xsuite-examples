#%%
import json
import xobjects as xo
import xpart as xp
import matplotlib.pyplot as plt
import numpy as np
import optics.toy_lattice as otl

import sys
import os
sys.path.insert(0, os.path.relpath('../../'))
from foil import Foil

#%%
####################################################################
# PSB foil
####################################################################
thickness = 200 #ug/cm^2
psbfoil = Foil(-1, 1, -1, 1, thickness)

#%%
####################################################################
# Initialize test bunch
####################################################################
with open('../testparticles.json', 'r') as fid:
    particles = xp.Particles.from_dict(json.load(fid), _context=xo.ContextCpu())
npart = len(particles.x) # 500
particles.x = np.zeros(npart)
particles.y = np.zeros(npart)
particles.px = np.zeros(npart)
particles.py = np.zeros(npart)
particles.zeta = np.zeros(npart)
particles.delta = np.zeros(npart)


#%%
####################################################################
# Cross foil
####################################################################
scatter_option = 1 # 1: simple (no losses) 0: full (with losses)
psbfoil.setScatterChoice(scatter_option)
turns = int(int(1e6)/npart)
_px = np.zeros(int(1e6))
_py = np.zeros(int(1e6))
for i in range(turns):
    print(i)
    psbfoil.track(particles)
    _px[(i*npart):((i+1)*npart)] = particles.px
    _py[(i*npart):((i+1)*npart)] = particles.py
    particles.px = np.zeros(npart)
    particles.py = np.zeros(npart)
_px = list(np.array(_px).ravel())
_py = list(np.array(_py).ravel())
d = {'scatter_option': scatter_option, '_px': _px, '_py': _py}
with open('kicks_myfoil_simple.json','w') as fid:
	json.dump(d,fid)
# %%
