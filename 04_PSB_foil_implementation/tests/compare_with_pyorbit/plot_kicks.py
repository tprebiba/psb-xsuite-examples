#%%
import numpy as np
import matplotlib.pyplot as plt
import json


#%%
with open('kicks_myfoil_simple.json','r') as fid:
    dmyfoil = json.load(fid)
with open('kicks_pyorbitfoil_simple.json','r') as fid:
    dpyorbit = json.load(fid)


# %%
f,axs = plt.subplots(1,2, figsize=(10,5))
fontsize=15
bins = np.linspace(-0.8,0.8,500)
f.suptitle(r'PSB foil, 200 ug/cm$^2$, simple scatter', fontsize=fontsize)
for i,plane in enumerate(['x','y']):
    ax = axs[i]
    ax.set_xlabel(f'$p_{plane}$ [mrad]', fontsize=fontsize)
    ax.set_ylabel('Hits', fontsize=fontsize)
    ax.hist(np.array(dpyorbit[f'_p{plane}'])*1e3, bins=bins, label='PyOrbit')
    ax.tick_params(axis='both', labelsize=fontsize)
    ax.tick_params(axis='y', left=False, labelleft=False)
    #ax.hist(np.array(dmyfoil[f'_p{plane}'])*1e3, bins=bins, label='Python', alpha=0.5)
    hist,bin_edges = np.histogram(np.array(dmyfoil[f'_p{plane}'])*1e3, bins=bins)
    ax.plot(bin_edges[:-1], hist, label='Python', c='orange')
    ax.legend(fontsize=fontsize)
    ax.set_xlim(-0.5,0.5)
f.tight_layout()
# %%
