#%%
import glob
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

#%%
source_dir = 'outputs/tbt/'
files = glob.glob(source_dir+'*.json')
print(files)

#%%
df = {}
for file in files:
    print(file)
    turn = int(file.split('/')[-1].split('_')[-1][0:5])
    df[turn] = pd.read_json(file)

#%%
turns = list(df.keys())
turn = turns[28]
print(turn)
plt.plot(df[turn]['y']*1e3, df[turn]['py']*1e3, '.', ms=2.0)
# %%
