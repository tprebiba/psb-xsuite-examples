#%%
import xtrack as xt
import xpart as xp
import xobjects as xo
import xfields as xf

import numpy as np
import matplotlib.pyplot as plt
from cpymad.madx import Madx

import json


#%%
#############################
# Load PSB line
#############################
context = xo.ContextCpu()
line = xt.Line.from_json('psb/psb_line.json')
line.build_tracker(_context=context)


#%%
#############################
# Prepare tune ramp
#############################
qx_target_fin = 4.15
qy_target_fin = 4.45
line.match(
              vary=[
                    xt.Vary('kbrqf', step=1e-8),
                    xt.Vary('kbrqd', step=1e-8),
              ],
              targets = [
                         xt.Target('qx', qx_target_fin, tol=1e-5),
                         xt.Target('qy', qy_target_fin, tol=1e-5)
              ]
)
kf_fin = line.vars['kbrqf']._value
kd_fin = line.vars['kbrqd']._value

qx_target_ini = 4.15
qy_target_ini = 4.65
line.match(
              vary=[
                    xt.Vary('kbrqf', step=1e-8),
                    xt.Vary('kbrqd', step=1e-8),
              ],
              targets = [
                         xt.Target('qx', qx_target_ini, tol=1e-5),
                         xt.Target('qy', qy_target_ini, tol=1e-5)
              ]
)
kf_ini = line.vars['kbrqf']._value
kd_ini = line.vars['kbrqd']._value
twbefore = line.twiss().to_pandas()


#%%
#############################
# Correct chroma
#############################
line.match(vary=[xt.Vary('kbr1xnoh0', step=1e-8)],
           targets = [xt.Target('dqy', 0.0, tol=1e-5)])


#%%
#############################
# Add half-integer excitation
#############################
line.vars['kbr1qno816l3'] = -6.15363e-4
line.vars['kbr1qno412l3'] = -6.15363e-4


#%%
######################
# Generate particles #
######################
n_part = int(5e2)
particles = xp.build_particles(_context=context, 
                               line=line,
                               particle_ref=line.particle_ref,
                               x=np.random.normal(0, 2e-3, n_part),
                               px=np.random.normal(0, 1.1e-3, n_part),
                               y=np.random.normal(0, 2e-3, n_part),
                               py=np.random.normal(0, 1.1e-3, n_part),
                               zeta=np.random.uniform(-1e-2, 1e-2, n_part),
                               delta=np.random.uniform(-1e-4, 1e-4, n_part),
                               method='4d')


#%%
#############################
# Start tracking
#############################
num_turns = 10000
for ii in range(num_turns):

      print(ii)

      # ramp quadrupoles
      _kf = kf_ini + (kf_fin-kf_ini)*ii/num_turns
      _kd = kd_ini + (kd_fin-kd_ini)*ii/num_turns
      line.vars['kbrqf'] = _kf
      line.vars['kbrqd'] = _kd
      
      # track one turn
      line.track(particles, 
                 turn_by_turn_monitor=True)
      
      # save every 100 turns
      if ii%500==0:
            print(f'save turn {ii}')
            with open(f'outputs/tbt/turn_{ii:05d}.json', 'w') as fid:
                  json.dump(particles.to_dict(), fid, cls=xo.JEncoder)