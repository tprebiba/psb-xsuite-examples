#%%
from cpymad.madx import Madx
import xtrack as xt
import xpart as xp
import xobjects as xo
import xfields as xf


#%%
context = xo.ContextCpu()
mad = Madx()
mad.globals.thin = 1
mad.globals.slices = 3
mad.call('psb_flat_bottom.madx')
line= xt.Line.from_madx_sequence(mad.sequence['psb1'],
                                 apply_madx_errors=True,
                                 deferred_expressions=True)
line.particle_ref=xp.Particles(mass0=xp.PROTON_MASS_EV,gamma0=mad.sequence.psb1.beam.gamma)
line.to_json('psb/psb_line.json')
