import numpy as np

from cpymad.madx import Madx

import xtrack as xt
import xpart as xp


kill_fringes_and_edges = False

bumper_names = ['bi1.bsw1l1.1', 'bi1.bsw1l1.2', 'bi1.bsw1l1.3', 'bi1.bsw1l1.4']
thick_bumpers = {
'bi1.bsw1l1.1' : {'k0_name': 'k0BI1BSW1L11'},
'bi1.bsw1l1.2' : {'k0_name': 'k0BI1BSW1L12'},
'bi1.bsw1l1.3' : {'k0_name': 'k0BI1BSW1L13'},
'bi1.bsw1l1.4' : {'k0_name': 'k0BI1BSW1L14'},
}

mad = Madx()

mad.input('''
    call, file = 'psb/psb.seq';
    call, file = 'psb/psb_fb_lhc.str';
    call, file = 'psb/psb_aperture.dbx';
    beam, particle=PROTON, pc=0.5708301551893517;
    use, sequence=psb1;
    twiss;
''')

mad.input('''
    QH = 4.4;
    QV = 4.45;

    MATCH, Sequence=psb1;
        VARY, NAME = kbrqf, STEP = 1e-3;
        VARY, NAME = kbrqd, STEP = 1e-3;
        GLOBAL, Q1 = QH;
        GLOBAL, Q2 = QV;
        JACOBIAN,CALLS=1000,TOLERANCE=1.0E-18,STRATEGY=3;
    ENDMATCH;
    ''')

line = xt.Line.from_madx_sequence(mad.sequence.psb1, allow_thick=True)
line.particle_ref = xp.Particles(mass0=xp.PROTON_MASS_EV,
                                gamma0=mad.sequence.psb1.beam.gamma)
line.build_tracker()

seq = mad.sequence.psb1
if kill_fringes_and_edges:
    for ee in seq.expanded_elements:
        print(ee.name)
        if hasattr(ee, 'KILL_ENT_FRINGE'):
            ee.KILL_ENT_FRINGE = True
        if hasattr(ee, 'KILL_EXI_FRINGE'):
            ee.KILL_EXI_FRINGE = True

    for ee in line.elements:
        if isinstance(ee, xt.DipoleEdge):
            ee.r21 = 0
            ee.r43 = 0

beta0 = line.particle_ref.beta0[0]

twmad = mad.twiss()
tw = line.twiss(method='4d')
dqx_mad = twmad.summary.dq1 * beta0
dqy_mad = twmad.summary.dq2 * beta0

print(f'dqx_mad =     {dqx_mad}')
print(f'dqx_xsuite =  {tw.dqx}')
print(f'dqy_mad =     {dqy_mad}')
print(f'dqy_xsuite =  {tw.dqy}')
