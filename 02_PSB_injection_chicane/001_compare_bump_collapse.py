import numpy as np

from cpymad.madx import Madx

import xtrack as xt
import xpart as xp

bumper_names = ['bi1.bsw1l1.1', 'bi1.bsw1l1.2', 'bi1.bsw1l1.3', 'bi1.bsw1l1.4']
thick_bumpers = {
'bi1.bsw1l1.1' : {'k0_name': 'k0BI1BSW1L11'},
'bi1.bsw1l1.2' : {'k0_name': 'k0BI1BSW1L12'},
'bi1.bsw1l1.3' : {'k0_name': 'k0BI1BSW1L13'},
'bi1.bsw1l1.4' : {'k0_name': 'k0BI1BSW1L14'},
}
tw_xsuite = []
qx_mad= []
qy_mad= []
qx_ptc= []
qy_ptc= []
qx_xsuite= []
qy_xsuite= []
dqx_mad = []
dqy_mad = []
dqx_xsuite = []
dqy_xsuite = []
k0_first_slice = []
r43_first_edge = []
k2_first_slice = []
for ii in range(1, 52, 5):
    print(f'ii = {ii}')

    mad = Madx()

    mad.input('''
        call, file = 'psb/psb.seq';
        call, file = 'psb/psb_fb_lhc.str';
        call, file = 'psb/psb_aperture.dbx';
        beam, particle=PROTON, pc=0.5708301551893517;
        use, sequence=psb1;
        twiss;
    ''')

    # Store bumpers length
    for nn in bumper_names:
        thick_bumpers[nn]['length'] = mad.sequence.psb1.expanded_elements[nn].l


    mad.input('''
        QH = 4.4;
        QV = 4.45;

        MATCH, Sequence=psb1;
            VARY, NAME = kbrqf, STEP = 1e-3;
            VARY, NAME = kbrqd, STEP = 1e-3;
            GLOBAL, Q1 = QH;
            GLOBAL, Q2 = QV;
            JACOBIAN,CALLS=1000,TOLERANCE=1.0E-18,STRATEGY=3;
        ENDMATCH;
        ''')

    mad.input(f'''
        use, sequence=psb1;
        call, file = 'psb/injection_chicane/setBSW.madx';
        readtable, file="psb/injection_chicane/BSW_collapse.tfs";

        SETVARS,TABLE=BSWTABLE,ROW={ii}; // Starts from 1 !!!

        exec, assign_BSW_strength;
        exec, assign_BSW_alignment;
        value, BSW_K0L;
    ''')

    mad.input('''
    beta=sqrt(1-1/beam->gamma^2);
    dispx:=beta*table(ptc_twiss,disp1);
    ptc_create_universe;
    ptc_create_layout, model=2, method=2, nst=5, exact=true;
    ptc_setswitch, debuglevel=0, nocavity=false, fringe=true,
                exact_mis=true, time=true, totalpath=true;
    PTC_ALIGN;
    ptc_twiss, closed_orbit, table=ptc_twiss, icase=4, no=2, summary_table=ptc_twiss_summary;
    ptc_end;
    ''')
    tw_ptc_thick = mad.table.ptc_twiss.dframe()
    tw_mad_thick = mad.twiss().dframe()
    dqx_mad.append(mad.table.summ.dq1)
    dqy_mad.append(mad.table.summ.dq2)

    mad.input('''
        seqedit, sequence = psb1;
        flatten;
        refer=centre;
        endedit;
        use, sequence=psb1;

        select, flag = MAKETHIN, clear;
        select, flag=MAKETHIN, SLICE=10, thick=False; ! thick=True keeps translated rbend as thick sbend
        MAKETHIN, SEQUENCE=psb1, STYLE=TEAPOT, MAKEDIPEDGE=True;
        use, sequence=psb1;
        exec, assign_BSW_strength;
        exec, assign_BSW_alignment;
        value, BSW_K0L;
        twiss;
    ''')

    # We modify the edge element on the thin lattice to fix issue with makethin

    for ii, nn in enumerate(bumper_names):
        ll = thick_bumpers[nn]['length']
        k0_name = thick_bumpers[nn]['k0_name']
        mad.input(
        f"{nn}_den, e1 := {k0_name}*{ll}/2, h := {k0_name} ;"
        f"{nn}_dex, e1 := {k0_name}*{ll}/2, h := {k0_name} ;"
        )
    # We have to set the angle ot the multipoles to a small number to avoid
    # MAD-X assumption angle=knl[0] which is used when angle is 0
    ee_thin_names = mad.sequence.psb1.element_names()
    for nn in thick_bumpers.keys():
        for ee_nn in ee_thin_names:
            if ee_nn.startswith(nn+'..'):
                mad.sequence.psb1.expanded_elements[ee_nn].angle = 1e-20

    line = xt.Line.from_madx_sequence(mad.sequence.psb1,
                                    apply_madx_errors=True,
                                    deferred_expressions=True)
    line.twiss_default['method'] = '4d'
    line.particle_ref = xp.Particles(mass0=xp.PROTON_MASS_EV, q0=1.0,
                                    gamma0=mad.sequence.psb1.beam.gamma)
    line.build_tracker()

    # Remove horizontal component of edges
    for nn in bumper_names:
        for suffix in ['_den', '_dex']:
            line[nn+suffix].r21 = 0

    # Scale v component of edges
    assert np.abs(line.vars['bsw_k0l']._value > 0.0000000001)
    bsw_k0l_ref = line.vars['bsw_k0l']._value
    for nn in bumper_names:
        for suffix in ['_den', '_dex']:
            r43 = line[nn+suffix].r43
            line.element_refs[nn+suffix].r43 = (
                r43 * line.vars['bsw_k0l'] / bsw_k0l_ref)

    tw = line.twiss()
    tw_xsuite.append(tw)

    qx_mad.append(tw_mad_thick.mux[-1])
    qy_mad.append(tw_mad_thick.muy[-1])
    qx_ptc.append(tw_ptc_thick.mu1[-1])
    qy_ptc.append(tw_ptc_thick.mu2[-1])
    qx_xsuite.append(tw.mux[-1])
    qy_xsuite.append(tw.muy[-1])

    dqx_xsuite.append(tw.dqx)
    dqy_xsuite.append(tw.dqy)

    k0_first_slice.append(line['bi1.bsw1l1.1..1'].knl[0])
    r43_first_edge.append(line['bi1.bsw1l1.1_den'].r43)
    k2_first_slice.append(line['bi1.bsw1l1.1..1'].knl[2])



    # import matplotlib.pyplot as plt
    # plt.close('all')
    # plt.figure(1)
    # plt.plot(tw_ptc_thick.s, tw_ptc_thick.x, label='PTC thick')
    # plt.plot(tw_mad_thick.s, tw_mad_thick.x, label='MAD thick')
    # plt.plot(tw.s, tw.x, label='Xsuite thin')

    # # Tunes
    # print(f'PTC thick tunes:   {tw_ptc_thick.mu1[-1]}\t{tw_ptc_thick.mu2[-1]}')
    # print(f'MAD thick tunes:   {tw_mad_thick.mux[-1]}\t{tw_mad_thick.muy[-1]}')
    # print(f'Xsuite thin tunes: {tw.mux[-1]}\t{tw.muy[-1]}')

    # plt.show()

import matplotlib.pyplot as plt
plt.close('all')
plt.figure(1)
plt.plot(qy_mad, label='MAD thick')
plt.plot(qy_ptc, label='PTC thick')
plt.plot(qy_xsuite, label='Xsuite thin')
plt.ylabel('Qy')
plt.legend()

plt.figure(2)
plt.subplot(211)
plt.plot(dqx_mad, label='MAD thick')
plt.plot(dqx_xsuite, label='Xsuite thin')
plt.ylabel("Q'x")
plt.legend()
plt.subplot(212)
plt.plot(dqy_mad, label='MAD thick')
plt.plot(dqy_xsuite, label='Xsuite thin')
plt.ylabel("Q'y")
plt.show()