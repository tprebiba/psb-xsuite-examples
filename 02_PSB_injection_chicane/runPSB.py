#%%
import xtrack as xt
import xpart as xp
import xobjects as xo
import xfields as xf

import numpy as np
import matplotlib.pyplot as plt
from cpymad.madx import Madx

import optics.psb_lattice as psb

import tfs


#%%
####################
# Choose a context #
####################
context = xo.ContextCpu()
# context = xo.ContextCupy()
#context = xo.ContextPyopencl('0.0')
print(context)


#%%
#############################
# Load PSB line
#############################
line = xt.Line.from_json('psb/psb_line.json')
line.build_tracker(_context=context)


#%%
#############################
# Unassign BSW pertrubations
#############################
line.vars['bsw_k0l'] = 0.0
line.vars['bsw_k2l'] = 0.0
bety_ref = line.twiss()['bety', 'mymarker']
alfy_ref = line.twiss()['alfy', 'mymarker']


#%%
#############################
# Match tunes and Twiss it
#############################
qx_target = 4.40
qy_target = 4.45
line.match(
              vary=[
                    xt.Vary('kbrqf', step=1e-8),
                    xt.Vary('kbrqd', step=1e-8),
              ],
              targets = [
                         xt.Target('qx', qx_target, tol=1e-5),
                         xt.Target('qy', qy_target, tol=1e-5)
              ]
)
twbefore = line.twiss().to_pandas()


#%%
#############################
# Assign chicane
#############################
bswdf = tfs.read("psb/injection_chicane/BSW_collapse.tfs")
assign_chicane_tablerow = 0
line.vars['bsw_k0l'] = bswdf.BSW_K0L[0]
line.vars['bsw_k2l'] = bswdf.BSW_K2L[0]
twafter = line.twiss().to_pandas()


#%%
#############################
# Match optics
#############################
line.match(verbose=True,
              vary=[
                    xt.Vary('kbrqd3corr', step=1e-5, limits=[-0.01, 0.0]),
                    xt.Vary('kbrqd14corr', step=1e-5, limits=[-0.01, 0.0]),
              ],
              targets = [
                         xt.Target('bety', at='mymarker', value=bety_ref, tol=1e-4, scale=1),
                         xt.Target('alfy', at='mymarker', value=alfy_ref, tol=1e-4, scale=1)
                         
              ]
)
twcorrected = line.twiss().to_pandas()
print(line.vars['kbrqd3corr']._value)
print(line.vars['kbrqd14corr']._value)

#%%
plt.plot(twbefore.s, twbefore.bety, label='unperturbed')
plt.plot(twafter.s, twafter.bety, label='perturbed')
plt.plot(twcorrected.s, twcorrected.bety, label='corrected')
plt.legend(loc=0)
plt.show()