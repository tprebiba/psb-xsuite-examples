#%%
import numpy as np
import matplotlib.pyplot as plt
import json
import pandas as pd
from scipy.interpolate import interp1d


#%%
# ##################################
# # Compare perturbed optics
# ##################################
# f,ax = plt.subplots(1,1,figsize=(10,5))
# fontsize=15
# ax.set_xlabel('s [m]',fontsize=fontsize)
# ax.tick_params(labelsize=fontsize)
# ctime = 'C277.5' # 'C275.0' or 'C277.5'
# df_ptctwiss = pd.read_pickle(f'outputs/perturbed_optics_{ctime}_ptctwiss.pkl')
# df_xsuite = pd.read_pickle(f'outputs/perturbed_optics_{ctime}_xsuite.pkl')
# ax.set_ylabel(r'$\beta_y$ (m)', fontsize=fontsize)
# ax.plot(df_ptctwiss.s, df_ptctwiss.bety, c='blue', label='ptc_twiss')
# ax.plot(df_xsuite.s, df_xsuite.bety, c='green', label='xsuite')
# #ax.set_ylabel(r'$\delta \beta_y$ (m)', fontsize=fontsize)
# #f1 = interp1d(df_xsuite.s, df_xsuite.bety, kind='linear', fill_value='extrapolate')
# #ax.plot(df_ptctwiss.s, df_ptctwiss.bety-f1(df_ptctwiss.s), c='black', label='ptc_twiss - xsuite')
# ax.set_xlim(0,130)
# ax.legend(loc=0, fontsize=fontsize)
# f.tight_layout()
# #f.savefig(f'outputs/optics_{ctime}.png', dpi=400)
# #f.savefig(f'outputs/delta_optics_ctime.png', dpi=400)


#%%
##################################
# Compare tunes
##################################
d = {}
keys = ['ptctwiss', 'xsuite']
for key in keys:
    with open(f'outputs/tunes_C275-C280_{key}.json', 'r') as fid:
        d[key] = json.load(fid)
f,ax = plt.subplots(1,1,figsize=(10,5))
fontsize=15
ax.set_xlabel('Ctime [ms]',fontsize=fontsize)
ax.set_ylabel(r'dQ (ptc_twiss - xsuite)', fontsize=fontsize)
ax.tick_params(labelsize=fontsize)
ax.axvline(275, ls='--',lw=1.5, c='grey')
ax.text(275, 4.455, 'Injection', rotation=90, c='grey', fontsize=fontsize)
#ax.axhline(0, ls='--',c='black')
#for c,key,lbl in zip(['b','g'],['qx','qy'],['Horizontal', 'Vertical']):
#for c,key,lbl in zip(['g'],['qy'],['Vertical']):
#    ax.plot(d['ptctwiss']['ctimes'], np.array(d['ptctwiss'][key]),c=c, ls='--', label=f'ptc_twiss ({lbl})')
#    ax.plot(d['xsuite']['ctimes'], np.array(d['xsuite'][key])-4,c=c, ls='-', label=f'xsuite ({lbl})')
#    ax.plot(d['ptctwiss']['ctimes'], np.array(d['ptctwiss'][key])-np.array(d['xsuite'][key])+4, 
#            c=c, ls='-',lw=2,label=lbl)
ax.set_ylabel(r'$Q_y$', fontsize=fontsize)
ax.plot(d['ptctwiss']['ctimes'], np.array(d['ptctwiss']['qy'])+4,c='blue', ls='-', label=f'ptc_twiss',lw=3)
ax.plot(d['xsuite']['ctimes'], np.array(d['xsuite']['qy']),c='green', ls='-', label=f'xsuite', lw=3)
ax.legend(loc=0, fontsize=fontsize)
f.savefig('outputs/tune_evolution.png', dpi=400)
f.tight_layout()


#%%
# ##################################
# # Compare full k-modulation
# ##################################
# d = {}
# keys = ['ptctwiss', 'xsuite']
# #keys = ['xsuite']
# for key in keys:
#     with open(f'outputs/output_{key}.json', 'r') as fid:
#         d[key] = json.load(fid)
# f,ax = plt.subplots(1,1,figsize=(10,5))
# fontsize=15
# ax.set_xlabel('CTime [ms]',fontsize=fontsize)
# ax.set_ylabel(r'$\bar{\beta_y}$ at QDE3 [m]', fontsize=fontsize)
# ax.tick_params(labelsize=fontsize)
# ax.axvline(275, ls='--',lw=1.5, c='grey')
# ax.text(275, 13, 'Injection', rotation=90, c='grey', fontsize=fontsize)
# for key,c in zip(keys,['blue','green']):
#     #ax.plot([d[key]['ctimes'][0],d[key]['ctimes'][-1]], [d[key]['meanbety_ini'], d[key]['meanbety_ini']], c='grey', ls='--')
#     #ax.plot(d[key]['ctimes'], d[key]['target_betas'], '-', c='blue')
#     ax.plot(d[key]['ctimes'], d[key]['measured_betas'], c=c, lw=3, label=key)
# ax.legend(loc=0, fontsize=fontsize)
# f.tight_layout()
# f.savefig('outputs/betay_atQDE3_evolution.png', dpi=400)
# # %%
