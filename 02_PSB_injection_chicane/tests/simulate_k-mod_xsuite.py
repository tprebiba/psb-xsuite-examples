#%%
import matplotlib.pyplot as plt
import numpy as np
import tfs
import xpart as xp
import xobjects as xo
import xfields as xf
import xtrack as xt
from scipy.optimize import curve_fit
import functools
import json
import pandas as pd


#%%
def Q_func(Delta_KL,beta,Q0,plane):
	Q0_2pi = 2*np.pi*Q0
	sgn = {'x': 1., 'y': -1.}[plane]
	Qint = np.floor(Q0)
	return np.arccos(np.cos(Q0_2pi)-0.5*sgn*beta*Delta_KL*np.sin(Q0_2pi))/(2*np.pi) + Qint

def beta_func(Delta_KL,Q,Q0,plane):
	Q0_2pi = 2*np.pi*Q0
	sgn = {'x': 1., 'y': -1.}[plane]
	beta = sgn*2./Delta_KL*(1./np.tan(Q0_2pi)*(1-np.cos(2*np.pi*(Q-Q0)))+np.sin(2*np.pi*(Q-Q0)))
	return beta

Q_func_y = functools.partial(Q_func, plane='y')
beta_func_y = functools.partial(beta_func, plane='y')

def fit_beta_function(delta_k_arr,l_quad,Qy_arr):
	delta_k_arr = np.atleast_1d(delta_k_arr)
	Qy_arr = np.atleast_1d(Qy_arr)
	Q0_guess = np.mean(Qy_arr[np.argsort(np.abs(delta_k_arr))][:3])
	popt, pcov = curve_fit(Q_func_y, delta_k_arr*l_quad, Qy_arr, p0=[14,Q0_guess])
	perr = np.sqrt(np.diag(pcov))
	betay0_fitted = popt[0]
	betay0_err = perr[0]
	Q0_fitted = popt[1]
	Q0_err = perr[1]
	delta_k_arr_sorted = np.sort(delta_k_arr)
	Qy_fitted = Q_func_y(delta_k_arr_sorted*l_quad,*popt)
	return (betay0_fitted,Q0_fitted,betay0_err,Q0_err,Qy_fitted,delta_k_arr_sorted)

def average_beta(K1,L,alfa0,beta0, plane):

    gamma0 = (1 + alfa0 ** 2) / beta0

    # for a defocusing quad (PSB specific): to be checked again
    if plane == 'y':
        u0 = 0.5*(1+np.sin(2*np.sqrt(K1)*L)/(2*np.sqrt(K1)*L))
        u1 = np.sin(np.sqrt(K1)*L)**2/(K1*L)
        u2 = 0.5/K1*(1-np.sin(2*np.sqrt(K1)*L)/(2*np.sqrt(K1)*L))
    elif plane == 'x':
        u0 = 0.5*(1 + np.sinh(2 * np.sqrt(K1) * L) / (2 * np.sqrt(K1) * L))
        u1 = np.sinh(np.sqrt(K1) * L) ** 2 / (K1 * L)
        u2 = -0.5 / K1 * (1 - np.sinh(2 * np.sqrt(K1) * L) / (2 * np.sqrt(K1) * L))

    # +u1 because twiss gives alfa0, beta0 at the end of the element (propagate backwards)
    avg_beta = beta0*u0 + alfa0*u1 + gamma0*u2
    return avg_beta

L = 0.87894



#%%
#############################
# Get PSB line for Xsuite
#############################
line = xt.Line.from_json('../psb/psb_line.json')
line.build_tracker(_context=xo.ContextCpu())
line.vars['bsw_k0l'] = 0.0
line.vars['bsw_k2l'] = 0.0
line.vars['kbrqd3corr'] = 0
line.vars['kbrqd14corr'] = 0
qx_target = 4.40
qy_target = 4.45
line.match(
    vary=[xt.Vary('kbrqf', step=1e-8),xt.Vary('kbrqd', step=1e-8)],
    targets = [xt.Target('qx', qx_target, tol=1e-8),xt.Target('qy', qy_target, tol=1e-8)]
)
twini = line.twiss().to_pandas()
kbrqf_ref = line.vars['kbrqf']._value
kbrqd_ref = line.vars['kbrqd']._value
element = 'br.qde3..3' # 3: at the end of the element
indx_quad = [i for i,n in enumerate(twini['name']) if element in n] 
k10 = line.element_dict[element].knl[1]/L
alfa0 = twini['alfy'][indx_quad] 
beta0 = twini['bety'][indx_quad]
meanbety_ini = float(average_beta(abs(k10),L,alfa0,beta0,'y')) # propagates backwards


#%%
#############################
# Get beta-beating at C275
#############################
bswdf = tfs.read("../psb/injection_chicane/BSW_collapse.tfs")
assign_chicane_tablerow = 0
line.vars['bsw_k0l'] = bswdf.BSW_K0L[assign_chicane_tablerow]
line.vars['bsw_k2l'] = bswdf.BSW_K2L[assign_chicane_tablerow]
twpert = line.twiss().to_pandas()
twpert.to_pickle('outputs/perturbed_optics_C%1.1f_xsuite.pkl'%(assign_chicane_tablerow/10+275))
print(line.twiss()['qx'], line.twiss()['qy'])


#%%
#############################
# Get tunes for C275-C280
#############################
bswdf = tfs.read("../psb/injection_chicane/BSW_collapse.tfs")
qx = []
qy = []
ctimes = []
for assign_chicane_tablerow in np.arange(0,51,1):
    ctimes.append(assign_chicane_tablerow/10+275)
    line.vars['bsw_k0l'] = bswdf.BSW_K0L[assign_chicane_tablerow]
    line.vars['bsw_k2l'] = bswdf.BSW_K2L[assign_chicane_tablerow]
    tw = line.twiss()
    qx.append(tw['qx'])
    qy.append(tw['qy'])
d = {'ctimes': ctimes,'qx':qx,'qy':qy}
with open('outputs/tunes_C275-C280_xsuite.json','w') as fid:
     json.dump(d, fid)


#%%
#####################################
# Simulate k-modulation measurement
#####################################
use_correctors = ['3']#, '14']
n_measurements = 10
Q_noise_std = 1e-3*0
dkminmax = 1.0
line.vars['bsw_k0l'] = 0.0
line.vars['bsw_k2l'] = 0.0
line.vars['kbrqf'] = kbrqf_ref
line.vars['kbrqd'] = kbrqd_ref
line.vars['kbrqd3corr'] = 0
line.vars['kbrqd14corr'] = 0
bswdf = tfs.read("../psb/injection_chicane/BSW_collapse.tfs")
ctimes = []
target_betas = []
measured_betas = []
measured_betas_err = []
#assign_chicane_tablerows = np.arange(0,5,1)
assign_chicane_tablerows = np.arange(0,51,1)
for assign_chicane_tablerow in assign_chicane_tablerows:
    ctime = assign_chicane_tablerow/10+275
    print(ctime)
    ctimes.append(ctime)
    line.vars['bsw_k0l'] = bswdf.BSW_K0L[assign_chicane_tablerow]
    line.vars['bsw_k2l'] = bswdf.BSW_K2L[assign_chicane_tablerow]
    twpert = line.twiss().to_pandas()

    for quad in use_correctors:
        element = 'br.qde%s..3'%quad # 3: at the end of the element
        indx_quad = [i for i,n in enumerate(twpert['name']) if element in n] 
        k10 = line.element_dict[element].knl[1]/L
        alfa0 = twpert['alfy'][indx_quad] 
        beta0 = twpert['bety'][indx_quad]
        meanbety_target = float(average_beta(abs(k10),L,alfa0,beta0,'y'))

        Qy_list = []
        delta_k_list = []
        bety_model_list = []
        for i in range(n_measurements):
            delta_k = np.random.uniform(-dkminmax,dkminmax)*1e-2
            delta_k_list.append(delta_k)
            line.vars[f'kbrqd{quad}corr'] = delta_k#*L
            Qy = np.copy(line.twiss()['qy'])
            Qy_list.append(Qy+np.random.normal(scale=Q_noise_std))
            #bety_model_list.append(tw['bety'][indx_quad])
        line.vars[f'kbrqd{quad}corr'] = 0

        _delta_k = np.array(delta_k_list)
        _Qy = np.array(Qy_list)
        res = fit_beta_function(_delta_k,L,_Qy)
        betay_fitted     = res[0]
        Q0_fitted        = res[1]
        betay_fitted_err = res[2]
        Q0_fitted_err    = res[3]
        y_fit_points     = res[4] # sorted
        x_fit_points     = res[5] # sorted

        if False:
            f, ax = plt.subplots(1, figsize=(4.85,3))
            ax.set_xlabel(r'$\delta k_{%s}$ ($10^{-3}$ m$^{-2}$)'%quad.upper())
            ax.set_ylabel(r'$Q_y$')
            ax.set_title(r'C%1.1f, $\beta_y^{target} = %1.2f$'%(assign_chicane_tablerow/10+275,meanbety_target))
            ax.plot(_delta_k*1e3, _Qy, 'o')
            ax.plot(x_fit_points*1e3, y_fit_points, label=r'$\beta_y^{fit}=%1.2f \pm %1.2f$'%(betay_fitted, betay_fitted_err))
            ax.legend(loc=0)
            plt.show()
    
    target_betas.append(meanbety_target)
    measured_betas.append(betay_fitted)
    measured_betas_err.append(betay_fitted_err)


# %%
plt.plot([ctimes[0], ctimes[-1]], [meanbety_ini, meanbety_ini], c='grey', ls='--')
plt.plot(ctimes, target_betas, '-', c='blue')
plt.errorbar(ctimes, measured_betas, yerr=measured_betas_err, c='red')
d = {
     'target_betas': list(target_betas),
     'measured_betas': list(measured_betas),
     'ctimes': list(ctimes),
     'meanbety_ini': float(meanbety_ini),
     'n_measurements': int(n_measurements),
     'Q_noise_std': float(Q_noise_std),
}
with open('output_xsuite.json','w') as fid:
     json.dump(d, fid)


# %%
